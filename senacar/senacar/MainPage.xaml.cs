﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace senacar
{
    public class Veiculo
    {
        public string Nome { get; set; }
    
        public float Preco { get; set; }
        public string PrecoFormatado
        {
            get { return string.Format("R$ {0}", Preco); }
        }
    }

    public partial class MainPage : ContentPage
    {
        public List<Veiculo> Veiculos{ get; set; }
        public MainPage()
        {
            InitializeComponent();

            this.Veiculos = new List<Veiculo>
            {
              new Veiculo { Nome = "Chevrolet Onix", Preco = 44000 },
              new Veiculo { Nome = "Hyundai Hb20", Preco = 42000 },
              new Veiculo { Nome = "Renault Sandro", Preco = 39000 },
              new Veiculo { Nome = "Ford Fiesta", Preco = 29000 },
              new Veiculo { Nome = "Honda Civic", Preco = 84000 },
            };
            this.BindingContext = this;
        }
    }
}      
